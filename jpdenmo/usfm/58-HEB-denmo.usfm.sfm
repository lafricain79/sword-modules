﻿\id HEB 58-HEB-denmo.usfm.sfm 電網聖書 giovedì 3 gennaio 2019
\ide UTF-8
\mt ヘブライ人への手紙 
\p
\p
\c 1
\v 1 神は，過去には，多くの場合に，またさまざまな方法で，預言者たちを通して父祖たちに語られましたが，
\v 2 これらの日々の終わりには，み子によってわたしたちに語られました。神は彼をすべてのものの相続人と定め，彼を通してもろもろの世界を造られました。
\v 3 み子はその方の栄光の輝き，その方の実体の生き写しであって，すべてのものをご自分の力ある言葉によって支えておられます。ご自身でわたしたちのための罪の清めを行なったのち，高い所で荘厳な方の右に座られました。
\v 4 み使いたちよりも優れた名を受け継いで，それだけ彼らにまさる者となられました。
\v 5 というのは，神はかつて，み使いたちのだれにこう言われたでしょうか。
\q1 「あなたはわたしの子，
\q2 わたしは今日あなたの父となった」。
\p
または，こう言われたでしょうか。
\q1 「わたしは彼の父となり，
\q2 彼はわたしの子となるだろう」。
\p
\v 6 さらに，この初子をこの世に連れ入れる時には，こう言われます。「神のみ使いたちはみな，彼を拝め」。
\v 7 み使いたちについては，こう言われます。
\q1 「そのみ使いたちを風にし，
\q2 その召使いたちを火の炎とされる方」。
\p
\v 8 一方，み子については，こう言われます。
\q1 「神よ，あなたのみ座は限りなく，
\q2 あなたの王国の笏は廉直の笏。
\q1
\v 9 あなたは義を愛し，不法を憎んだ。
\q2 それゆえに，神，あなたの神は，喜びの油をあなたの仲間にまさってあなたに注がれた」。
\p
\v 10 また，
\q1 「主よ，あなたははじめに地の基礎を置かれました。
\q2 もろもろの天はあなたのみ手の業です。
\q1
\v 11 それらは滅びることになりますが，あなたはとどまっておられます。
\q2 それらはみな外衣のように古くなるでしょう。
\q1
\v 12 あなたは外とうのようにそれらを巻かれます。
\q2 それらは取り替えられることになりますが，
\q1 あなたは同じであられます。
\q2 あなたの年が尽きることはありません」。
\p
\v 13 しかし，神はかつて，み使いたちのだれにこう言われたでしょうか。
\q1 「わたしの右に座っていなさい，
\q2 わたしがあなたの敵たちをあなたの足台とするまで」。
\p
\v 14 彼らはみな仕える霊であって，救いを受け継ぐ人たちに奉仕するために遣わされたのではありませんか。
\p
\c 2
\v 1 それゆえ，わたしたちは聞いた事柄にいっそう注意を払わなければなりません。そうでないと，わたしたちは押し流されてしまうかも知れません。
\v 2 というのは，み使いたちを通して語られた言葉が堅く保証され，すべての違反と不従順が当然の報いを受けたのであれば，
\v 3 わたしたちは，これほど大きな救いをないがしろにした場合，どうして逃れることができるでしょうか。この救いは最初に主を通して語られ，聞いた人たちによってわたしたちのために確証され，
\v 4 神も，しるしと不思議，さまざまな力ある業，またご自分のご意志に従って聖霊の贈り物を分け与えることにより，彼らと共に証言されたのです。
\v 5 というのは，その方は，わたしたちが語っている来たるべき世を，み使いたちに服させることはされなかったからです。
\v 6 むしろ，ある人がある箇所で証言して，こう言っています。
\q1 「人間は何者なのですか，あなたがそれに思いを留められるとは。
\q2 また，人の子は何者なのですか，あなたがそれを心にかけてくださるとは。
\q1
\v 7 あなたは彼をしばらくの間み使いたちよりも低い者とされ，
\q2 彼に栄光と誉れを冠として与えられました。\f + TRは，「そして彼をあなたのみ手の業の上に置かれました」と加えている。\f*
\q2
\v 8 すべてのものを彼の足の下に服従させられました」。
\p
その方は，すべてのものを彼に服させられたのですから，彼に服さないものは一つも残されなかったのです。それなのに，わたしたちはすべてのものが彼に服しているのをまだ見ていません。
\v 9 ただ，しばらくの間み使いたちよりも低くされた方であるイエスが，死の苦しみのゆえに，栄光と誉れを冠として与えられたのを見ています。それは，神の恵みによって，彼がすべての人のために死を味わわれるためでした。
\v 10 というのは，多くの子らを栄光に至らせるにあたって，彼らの救いの創始者を数々の苦しみを通して完成させることは，すべてのものがその方のために存在し，またすべてのものがその方を通して存在しているその方にふさわしいことであったのです。
\v 11 実に，聖別する方も聖別される人たちも，みな一人の方から出ているからです。そのために彼は，彼らを兄弟たち\f + この「兄弟たち」という言葉は，ここや文脈が許す箇所においては，正確には「兄弟たちと姉妹たち」または「きょうだいたち」とも訳せる。\f*と呼ぶことを恥とはされず，
\v 12 こう言っておられます。
\q1 「わたしはあなたのみ名をわたしの兄弟たちに知らせます。
\q2 会衆の中であなたの賛美を歌います」。
\p
\v 13 また，「わたしはその方に信頼を置きます」。また，「見よ，わたしと，神がわたしに与えてくださった子供たちがここにいる」。
\v 14 それで，子供たちは肉と血とにあずかっているので，彼自身も同じようにその同じものを共にされました。それは，ご自分の死を通して，死の力を持つ者，すなわち悪魔を無に至らせ，
\v 15 死についての恐れのために生涯にわたって束縛を受けていた者たちを，みな解放するためでした。
\v 16 確かに，彼はみ使いたちを助けるのではなく，アブラハムの子孫を助けてくださるからです。
\v 17 それで，彼はすべてのことにおいて自分の兄弟たちと同じようになる義務がありました。それは，民の罪をあがなうために，神に関わる事柄において恵み深い忠実な大祭司となるためでした。
\v 18 というのは，彼自身，誘惑に遭って苦しみを受けられたからこそ，誘惑に遭っている人たちを助けることができるのです。
\p
\c 3
\v 1 それゆえ，聖なる兄弟たち，天の召しにあずかっている人たちよ，わたしたちが告白する使徒また大祭司であるイエスのことをよく考えなさい。
\v 2 彼は自分を任命した方に忠実であられました。モーセもその方の家全体にあってそうであったのと同じです。
\v 3 彼はモーセよりも大きな栄光にふさわしいとみなされました。家を建てた人は家よりも大きな誉れを受けるからです。
\v 4 家はすべてだれかによって造られますが，すべてのものを造られたのは神なのです。
\v 5 確かにモーセはその方の家全体にあって召使いとして忠実でした。それは，将来語られる事柄について証言するためでした。
\v 6 しかし，キリストはその方の家を治めるみ子として忠実でした。わたしたちが確信と希望による誇りとを終わりまでしっかりと堅く保つなら，わたしたちこそその方の家なのです。
\v 7 それゆえ，聖霊が次のように言っているとおりです。
\q1 「今日，あなた方はその方の声を聞くなら，
\q1
\v 8 荒野での試練の日のように，
\q2 自分たちの心をかたくなにして，挑発してはいけない。
\q1
\v 9 その場所であなた方の父祖たちは試みをもってわたしを試し，
\q2 四十年間わたしの業を目にした。
\q1
\v 10 それでわたしはこの世代に腹を立てて
\q2 こう言った，『彼らはいつもその心のうちに迷っている。
\q2 彼らはわたしの道を知らなかった』。
\q1
\v 11 わたしは激しい怒りのうちに誓った，
\q2 『彼らがわたしの安息に入ることはない』」。
\p
\v 12 兄弟たち，あなた方のうちのだれも，不信仰な悪い心を抱いて生ける神から離れることがないよう，用心しなさい。
\v 13 むしろ，「今日」と呼ばれている間じゅう，あなた方のうちのだれも罪の欺きによってかたくなになることのないよう，日ごとに互いに勧め合いなさい。
\v 14 というのは，初めの確信を終わりまでしっかりと堅く保つなら，わたしたちはキリストにあずかる者となっているのです。
\v 15 ただし，次のように言われている間のことです。
\q1 「今日，あなた方はその方の声を聞くなら，
\q2 自分たちの心をかたくなにして，挑発してはいけない」。
\p
\v 16 というのは，聞いていながら反逆したのはいったいだれでしたか。そうです，モーセによってエジプトから出て来たすべての者たちではありませんでしたか。
\v 17 その方が四十年間立腹されたのはだれに対してでしたか。罪を犯して，その死体が荒野に倒れた者たちではありませんでしたか。
\v 18 その方がご自分の安息に入らせないと誓われたのは，不従順な者たち以外のだれに対してでしたか。
\v 19 こうしてわたしたちは，彼らが入れなかったのは不信仰のゆえであったことに気づきます。
\p
\c 4
\v 1 それゆえ，その方の安息に入るという約束が残されているのに，あなた方のうちのだれかがそれに達していないと思えるようなことのないように気遣いましょう。
\v 2 わたしたちは自分たちに宣教された良いたよりを持っているからです。それは彼らも同じことですが，聞いた言葉は彼らの益になりませんでした。聞いた人たちと信仰によって結び付かなかったからです。
\v 3 信じたわたしたちはその安息に入るのです。その方が，「わたしは激しい怒りのうちに誓った，彼らがわたしの安息に入ることはない」と言われたとおりです。しかも，み業は世の基礎が据えられて以来，すでに仕上がっていたのです。
\v 4 ある箇所で七日目についてこう言っておられるからです。「神は七日目にそのすべての業を休んだ」。
\v 5 そしてこの箇所で再び，「彼らがわたしの安息に入ることはない」と言われています。
\p
\v 6 それゆえ，それに入る者たちが残されており，先に良いたよりを宣教された者たちは不従順のゆえに入らなかったので，
\v 7 その方はある日を再び今日と定めて，（すでに言われているとおり）長い期間の後でダビデを通して次のように言われたのです。
\q1 「今日，あなた方はその方の声を聞くなら，
\q2 自分たちの心をかたくなにしてはいけない」。
\p
\v 8 もしヨシュアが彼らに安息を与えていたのであれば，その方は後になって別の日について語られることはなかったでしょう。
\v 9 それゆえ，神の民には安息日の休みが残されています。
\v 10 というのは，その方の休みに入った者は，神がみ業を休まれたように，自らも自分の業を休んだからです。
\v 11 ですから，その安息に入るために励みましょう。だれかが同じ不従順の例にならって堕落するようなことのないためです。
\v 12 というのは，神の言葉は生きていて，活発であり，あらゆる両刃の剣よりも鋭く，魂と霊，また関節と骨髄とを切り分けるほどに刺し通し，心の思いと意向とを見分けることができるのです。
\p
\v 13 その方のみ前に隠されている被造物は一つもなく，すべてのものはその方の目には裸であり，さらけ出されています。わたしたちはその方に対してことを果たさなければなりません。
\v 14 それでは，もろもろの天を通り抜けられた偉大な大祭司，神の子イエスがおられるのですから，わたしたちの告白を堅く保ちましょう。
\v 15 わたしたちの大祭司は，わたしたちの弱さを思いやることのできない方ではなく，罪を別にすれば，すべての点でわたしたちと同じように誘惑を受けられた方なのです。
\v 16 ですから，わたしたちは，あわれみを受けるため，また必要な時に助けとなる恵みを見いだすために，大胆さをもって恵みのみ座に近づきましょう。
\p
\c 5
\v 1 なぜなら，大祭司はすべて人間たちの中から選び取られ，人間たちのため，神に関する事柄について任命されるからです。それは，罪のための供え物と犠牲とをささげるためです。
\v 2 大祭司は，無知で迷っている者たちを穏やかに扱うことができます。自分自身も弱さにまとわれているからです。
\v 3 そのゆえに，民のためだけではなく自分自身のためにも罪のための犠牲をささげなければなりません。
\v 4 だれもこの栄誉を自分で得ることはなく，アロンもそうであったとおり，神から召されて受けるのです。
\v 5 同じようにキリストも，大祭司になる栄光を自分で得られたのではなく，彼に対して次のように言われた方がその栄光をお与えになったのです。
\q1 「あなたはわたしの子。
\q2 わたしは今日あなたの父となった」。
\p
\v 6 別の箇所でもこう言われているとおりです。
\q1 「あなたは永久に
\q2 メルキゼデクの位に等しい祭司」。
\p
\v 7 彼は肉体でおられた日々，自分を死から救うことのできる方に，強い叫びと涙をもって祈りと請願をささげ，その信心深い恐れのゆえに聞き入れられました。
\v 8 彼はみ子であったにもかかわらず，お受けになった数々の苦しみから従順を学ばれました。
\v 9 こうして完成させられて，ご自分に従う者たちすべてに対して永遠の救いの創始者となり，
\v 10 神によってメルキゼデクの位に等しい大祭司と名付けられたのです。
\v 11 彼については，わたしたちには言いたいことがたくさんありますが，説明するのが困難です。あなた方が，聞くことに関して鈍くなっているからです。
\v 12 あなた方は，時間の点からすれば教師になっているはずなのに，神の託宣の初歩的な基礎原理をだれかに教わることが，再び必要になっています。固い食物ではなく乳を必要とするようになっています。
\v 13 というのは，乳によって生きている者はみな，赤子であるため，義の言葉に熟達していないのです。
\v 14 一方，固い食物は，十分に成長した人々，すなわち，良いことと悪いことを見分けるために，使うことによって訓練された感覚を持っている人々のものです。
\p
\c 6
\v 1 ですから，わたしたちは，キリストについての初歩的な原理は後にして，完成へと押し進むことにし，次のような基礎を再び据えることはやめましょう。すなわち，死んだ業からの悔い改め，神に対する信仰，
\v 2 数々のバプテスマについての教え，手を置くこと，死んだ者たちの復活，そして永遠の裁きなどに関することです。
\v 3 神が許してくださるなら，わたしたちはこのことを行なうでしょう。
\v 4 というのは，いったん啓発を受け，天からの贈り物を味わい，聖霊にあずかる者となり，
\v 5 神の良い言葉と来たるべき世の力を味わっておきながら，
\v 6 その後で離れ落ちる者たちについては，彼らを再び悔い改めに立ち返らせるのは不可能なのです。神の子を自分たちで再びはりつけにして，彼を公の恥辱にさらしているからです。
\v 7 というのは，土地が，その上にたびたび降って来る雨を吸い込み，それを耕している人たちに役立つ作物を生み出すなら，神からの祝福を受けますが，
\v 8 イバラやアザミを生じるなら，それは退けられ，のろいに近づいており，その終わりには焼かれてしまいます。
\p
\v 9 しかし，愛する者たちよ，わたしたちはこのように語ってはいても，あなた方については，もっと良い事柄，また救いに伴う事柄があると確信しています。
\v 10 というのは，神は不義な方ではないので，あなた方の業や，あなた方が聖徒たちに仕え，また今も仕えていることによってみ名に示した愛の骨折りとを忘れることはされないからです。
\v 11 わたしたちは，あなた方ひとりひとりが，終わりに至るまで十分な希望を保つため，その同じ勤勉さを示して欲しいと思います。
\v 12 それは，あなた方が不活発になったりせず，約束を信仰と忍耐とによって受け継ぐ人たちの模倣者となるためです。
\v 13 神はアブラハムに約束されたとき，ご自分より偉大な者にかけて誓うことができなかったので，ご自身にかけて誓い，
\v 14 「わたしは必ずあなたを祝福し，必ずあなたを殖やすだろう」と言われたのです。
\v 15 こうしてアブラハムは，辛抱強く耐え忍んだのち，約束のものを獲得しました。
\v 16 というのは，実際，人間たちは自分より偉大な者にかけて誓うのであって，その誓いは確証となってあらゆる論争に決着をつけます。
\v 17 このように神は，約束の相続人たちにご自分の考えの不変性をさらに豊かに示そうと決めると，誓いによって介入されたのです。
\v 18 それは，自分の前に置かれた希望をつかもうとして避難所に逃れて来たわたしたちが，神が偽ることのできない二つの不変の事柄によって，力強い励ましを得るためでした。
\v 19 その希望を，わたしたちは魂の錨，確実でしっかりした希望，また垂れ幕の内側に入って行くものとして持っています。
\v 20 そこへは先駆者であるイエスがわたしたちのために入り，永久にメルキゼデクの位に等しい大祭司となられたのです。
\p
\c 7
\v 1 というのは，このメルキゼデク，すなわちサレムの王，いと高き神の祭司，王たちを撃滅して戻って来たアブラハムを迎えて祝福した人，
\v 2 アブラハムがすべての物の十分の一を分け与えた人ですが（その名を解釈すれば，第一に義の王であり，次いでサレムの王すなわち平和の王であって，
\v 3 父もなく，母もなく，系図もなく，日々の初めもなければ命の終わりもなく，神の子に似せられています），この人はずっと祭司のままです。
\v 4 さあ，この人がどれほど偉大であったかをよく考えなさい。族長のアブラハムでさえ，彼には最上の戦利品の中から十分の一を与えたのです。
\v 5 確かに，レビの子らのうち祭司職を受ける人々は，民から，すなわち彼らの兄弟たちの中から，同じアブラハムの腰から出て来た者たちであるにもかかわらず，十分の一税を取るよう，律法によっておきてを与えられています。
\v 6 ところが，彼らの系統に属さないこの人が，アブラハムから十分の一税を取って，約束を受けている彼を祝福したのです。
\v 7 さて，より劣った者がより優れた者から祝福を受けるということは，議論の余地のないことです。
\v 8 一方では死ぬ人々が十分の一税を受けていますが，他方では生きていると証言されている人が十分の一税を受けているのです。
\v 9 それで，十分の一税を受けるレビさえも，アブラハムを通して十分の一税を支払ったと言えます。
\v 10 メルキゼデクがアブラハムを迎えたとき，レビはまだ自分の父祖の腰にいたからです。
\v 11 ところで，もしレビの祭司職を通して完全にすることがあったとすれば（民はそのもとで律法を受けたのですが），どうしてこれ以上，アロンの位に等しいと呼ばれず，むしろメルキゼデクの位に等しい別の祭司が立てられる必要があるでしょうか。
\v 12 というのも，祭司職が変えられると，律法にも変更が必要になるのです。
\v 13 これらのことで言及されている方は別の部族に属しているのであり，その部族からはだれも祭壇での職務を行なってはきませんでした。
\v 14 なぜなら，わたしたちの主がユダ，すなわちモーセが祭司職については何も語らなかった部族から出たことは明らかだからです。
\v 15 このことは，メルキゼデクに似た別の祭司が立てられるなら，ますます明らかになります。
\v 16 その方は，肉的なおきての律法によらず，終わりのない命の力によって立てられました。
\v 17 次のように証言されているからです。
\q1 「あなたは永久に
\q2 メルキゼデクの位に等しい祭司」。
\p
\v 18 というのは，先行のおきてはその弱さと無益さのゆえに廃止されますが
\v 19 （律法は何をも完全にしなかったからです），その結果さらに優れた希望がもたらされたからです。それを通してわたしたちは神に近づいているのです。
\v 20 なぜなら，彼は誓いによらずに祭司になったのではなく
\v 21 （実際，人々は誓いなしで祭司になっているのですが），自分について次のように言われる方による誓いによっていたからです。
\q1 「主はこう誓われた。そしてその思いを変えることはされない。
\q2 『あなたは永久に
\q2 メルキゼデクの位に等しい祭司』」。
\p
\v 22 このようにして，イエスはより優れた契約の保証となられました。
\v 23 実際，人々は職務にとどまることを死によって阻まれるので，多くの人々が祭司になってきました。
\v 24 しかし，彼は永久に生きているので，不変の祭司職を持っておられます。
\v 25 それゆえ，彼はまた，ご自分を通して神に近づく者たちを完全に救うことができます。常に生きていて，彼らのために仲裁をしてくださるからです。
\p
\v 26 というのは，このような大祭司，神聖であり，罪もなく，汚れもなく，罪人たちから引き離されており，もろもろの天よりも高くなられた方こそ，わたしたちに適していたのです。
\v 27 彼は，あの大祭司たちのように，最初に自分自身の罪のために，次いで民の罪のために，日ごとに犠牲をささげる必要はありません。ご自身をささげた時，ただ一度だけこのことを行なわれたからです。
\v 28 なぜなら，律法は弱さを持つ人間たちを大祭司として任命しますが，律法の後に来た誓いの言葉は，永久に完成されているみ子を任命するからです。
\p
\c 8
\v 1 さて，わたしたちが述べてきた事柄の要点は，わたしたちにはこのような大祭司がおられるということです。その方は，天において荘厳な方のみ座の右に座られた方で，
\v 2 聖所の，また人間ではなく主が立てられた本物の幕屋の奉仕者です。
\v 3 というのは，すべての大祭司は供え物と犠牲とをささげるために任命されるからです。それで，この大祭司も何かささげるものを持っておられることが必要です。
\v 4 それで，もし彼が地上におられたなら，祭司ではなかったでしょう。地上には，律法に従って供え物をささげる祭司たちがいるからです。
\v 5 彼らは天にあるものの写しと影とに仕えています。それは，モーセが幕屋を造ろうとしていた時，神から告げられたとおりです。その方は，「さあ，山であなたに示された型に従って，すべてのものを造りなさい」と言われたのです。
\v 6 しかし今，わたしたちの大祭司は，さらに優れた奉仕の務めを獲得しておられます。それは，よりまさった約束に基づいて制定された，よりまさった契約の仲介者ともなられたことによります。
\v 7 というのは，あの最初の契約に欠点がなかったなら，第二のものが求められる余地はなかったことでしょう。
\v 8 彼らに欠点を見いだして，こう言われるからです。
\q1 「見よ，その日々がやって来る」と，主は言われる。
\q2 「そのとき，わたしはイスラエルの家およびユダの家と新しい契約を結ぶことになる。
\q1
\v 9 それは，その手を取って彼らの父祖たちをエジプトの地から導き出した日に，
\q2 わたしが彼らと結んだ契約によるものではない。
\q1 彼らはわたしの契約のうちにとどまらず，
\q2 わたしは彼らを無視したからだ」と，主は言われる。
\q1
\v 10 「これこそ，わたしがイスラエルの家と結ぶ契約だ。
\q2 それらの日々ののちに」と，主は言われる。
\q1 「わたしは自分の律法を彼らの思いの中に置き，
\q2 それを彼らの心の上に書きつけることになる。
\q1 わたしは彼らの神となり，
\q2 彼らはわたしの民となるだろう。
\q1
\v 11 彼らが，それぞれ自分の仲間の市民\f + TRは「仲間の市民」の代わりに「隣人」と読んでいる。\f*に，
\q2 それぞれ自分の兄弟に教えて，『主を知れ』と言うことは決してない。
\q2 最も小さな者から最も大きな者に至るまで，
\q2 すべての者がわたしを知るようになるからだ。
\q1
\v 12 わたしは，彼らの不義に対してあわれみ深い者となるからだ。
\q2 彼らの罪や不法な行ないをもはや思い出すことはないだろう」。
\p
\v 13 「新しい契約」と言われることによって，最初のものを古いものとされました。そして，古いものとされて年を経たものは，消滅に近づいているのです。
\p
\c 9
\v 1 ところで，最初の\f + TRは「幕屋の」を加えている。\f*契約にも，神的な奉仕の法令と地上の聖所とがありました。
\v 2 すなわち，幕屋が設けられたのです。第一の区画には，燭台と食卓と供えのパンがあり，聖なる場所と呼ばれていました。
\v 3 第二の垂れ幕の後ろには至聖所と呼ばれる幕屋がありました。
\v 4 そこには金の香の祭壇と，全面を金で覆われた契約の箱があり，その箱の中にはマンナを入れた金のつぼ，芽を出したアロンのつえ，そして契約の書き板がありました。
\v 5 また，その上には，あわれみの座を影で覆っている栄光のケルビムがありました。しかしわたしたちは今，こうしたことの詳細を述べるわけにはゆきません。
\v 6 さて，これらの物がこのように設けられると，第一の幕屋の中には，祭司たちが務めを果たすために絶えず入って行きますが，
\v 7 第二の区画には年に一度，大祭司だけが入って行くのであり，それも，自分自身のため，また民の過失のためにささげる血を携えないで行くことはありません。
\v 8 聖霊はこのことを，すなわち，第一の幕屋がなお立っている間は，聖なる場所への道がまだ現わされてはいなかったことを示しています。
\v 9 この第一の幕屋は今の時代の象徴です。そこでは供え物や犠牲がささげられますが，崇拝者を良心の面で完全にすることができません。
\v 10 それらは，改革の時まで課せられている，（食べ物や飲み物やさまざまな洗いに関する）肉的な法令に過ぎないのです。
\p
\v 11 しかし，キリストは，来たるべき善い事柄の大祭司として来られたとき，手で造ったのではない，つまり，この創造界に属さない，より偉大で，より完全な幕屋を通り，
\v 12 ヤギや子牛の血ではなく，ご自身の血を通して，ただ一度だけ聖なる場所に入り，永遠の請け戻しを獲得されたのです。
\v 13 汚れてしまった者たちに振りかけられる，ヤギや雄牛の血また若い雌牛の灰が，彼らを肉の清さのために聖別するのであれば，
\v 14 まして，永遠の霊によって，自らを汚点のないものとして神にささげたキリストの血は，あなた方を生ける神に仕えさせるために，あなた方の良心を死んだ業からどれほど清めてくれることでしょうか。
\v 15 このようなわけで，彼は新しい契約の仲介者です。それは，最初の契約のもとでの数々の違反を請け戻すために一つの死が生じたことにより，召された者たちが，約束された永遠の相続財産を受けられるようにするためです。
\v 16 というのは，遺言状のあるところには，それを残した人の死が必要なのです。
\v 17 遺言は死がもたらされたときに有効になるからであって，それを残した人が生きている間は決して有効にならないからです。
\v 18 それゆえ，最初の契約でさえ，血が伴わずに制定されたのではありません。
\v 19 というのは，モーセは，すべてのおきてを律法に基づいて民全員に語ったとき，子牛とヤギの血を取り，水と赤い羊毛とヒソプと共に，書そのものと民全体とに振りかけて，
\v 20 「これは神があなた方に命じられた契約の血だ」と言ったからです。
\p
\v 21 さらに彼は，幕屋とすべての務めの器に同じように血を振りかけました。
\v 22 律法によれば，ほとんどすべてのものが血をもって清められ，血を流すことがなければ許しはないのです。
\v 23 ですから，天にあるものの写しはこれらのものによって清められ，一方，天にあるもの自体は，これらのものよりもさらに優れた犠牲で清められることが必要でした。
\v 24 というのは，キリストは，本物の聖なる場所の描出である，手で造った聖なる場所ではなく，天そのものに入られたのであり，今やわたしたちのために神のみ前に現れてくださったからです。
\v 25 それは，大祭司が年ごとに自分のものではない血を携えて聖なる場所に入るように，何度もご自身をささげるためではありませんでした。
\v 26 そうだとしたなら，世の基礎が据えられて以来，何度も苦しみを受けなければならなかったでしょう。しかし今や，時代の終わりにただ一度，ご自身の犠牲によって罪を取り除くために現われてくださっているのです。
\v 27 人間たちには，ただ一度だけ死ぬこと，そしてそののちに裁きを受けることが定まっていますが，
\v 28 そのようにキリストも，多くの人たちの罪を担うためにただ一度だけささげられたのち，二度目には，罪とは関係なく，彼をひたすら待っている人たちに救いをもたらすために現われることになっています。
\p
\c 10
\v 1 律法は来たるべき善いことの影を持っているのであって，それ自体の姿は持っていないので，年ごとに絶えずささげられる同じ犠牲によって，近づく者たちを完全にすることは決してできません。
\v 2 もしできるとすれば，崇拝者たちはただ一度だけ清められ，もはや罪の意識を持たなくなるのですから，ささげ物はされなくなったはずではありませんか。
\v 3 ところが，これらの犠牲によって，罪が年ごとに思い出させられるのです。
\v 4 なぜなら，雄牛やヤギの血では罪を取り除くことは不可能だからです。
\v 5 ですから，彼はこの世に入って来る時，こう言われるのです。
\q1 「犠牲やささげ物をあなたは望まず，
\q2 わたしのために体を用意してくださいました。
\q1
\v 6 全焼のささげ物や罪のための犠牲をあなたは喜ばれませんでした。
\q2
\v 7 そこでわたしは言いました。『ご覧ください，わたしは参りました（書の巻き物にわたしについて書いてあります），
\q2 神よ，あなたのご意志を行なうために』」。
\p
\v 8 先に，「犠牲やささげ物，また全焼のささげ物や罪のための犠牲をあなたは望まず，それらを喜ばれませんでした」と言い（それらは律法によってささげられるものです），
\v 9 次いで，「ご覧ください，あなたのご意志を行なうために，わたしは参りました」と言われるのです。彼は，第二のものを確立するために，最初のものを取り除かれます。
\v 10 このご意志により，イエス・キリストの体という，ただ一度のささげ物を通して，わたしたちは聖別されているのです。
\v 11 実に，祭司はすべて，日ごとに立って務めを果たし，決して罪を取り除くことのできない同じ犠牲を何度もささげていますが，
\v 12 この方は，罪のために一つの犠牲を永久にささげてから，神の右に座り，
\v 13 それ以来，自分の敵たちが自分の足台とされるまで待っておられます。
\v 14 聖別されている人たちを，一つのささげ物によって，永久に完全にされたのです。
\v 15 聖霊も次のように言って，わたしたちに証言されます。
\q1
\v 16 「これこそ，わたしが彼らと結ぶ契約だ。
\q2 『それらの日々ののちに』と，主は言われる。
\q1 『わたしは自分の律法を彼らの心の上に置き，
それを彼らの思いの上に書きつけることになる』」。
\p
次いで，こう言われます。
\q1
\v 17 「わたしは彼らの罪や不法をもはや思い出すことはないだろう」。
\p
\v 18 それで，これらのことの許しがあるところには，もはや罪のささげ物はありません。
\v 19 それゆえ，兄弟たち，わたしたちは，イエスの血によって聖なる場所に入るための大胆さを抱いているのですから，
\v 20 （その道は，彼が，垂れ幕つまりご自身の肉を通して，わたしたちのために設けてくださった，新しい生きた道です。）
\v 21 そして，神の家を治める偉大な祭司がいるのですから，
\v 22 満ちあふれる信仰のうちに，真実の心をもって近づきましょう。わたしたちは，心に血を振りかけられて悪い良心から清められており，体を清い水で洗われているのですから，
\v 23 わたしたちの希望の告白を断固しっかりと保ちましょう。約束してくださったのは誠実な方だからです。
\p
\v 24 互いをどのように愛と善い業へと駆り立てるかをよく考え，
\v 25 わたしたち自身が一緒に集まり合うことを，ある人たちの習慣にならって途絶えさせたりせず，むしろ互いに励まし合い，その日が近づくのを見て，ますますそうしましょう。
\v 26 というのは，わたしたちが真理の知識を受けた後で，故意に罪を犯し続けるなら，罪のための犠牲はもはや残っておらず，
\v 27 ただ，裁きへの恐ろしい予想のようなものと，対抗者たちを焼き尽くす火の猛烈さだけが残っているのです。
\v 28 モーセの律法を無視する者は，二人か三人の証人の言葉に基づいて，あわれみを受けることなく死にます。
\v 29 まして，神の子を踏みつけ，自分がそれによって聖別された契約の血を神聖でないものとみなし，恵みの霊を侮辱する者は，どれほどひどい刑罰に値すると思いますか。
\v 30 わたしたちは次のように言われた方を知っているからです。「報復はわたしのもの」と，主は言われます。「わたしが返報する」。また，「主はご自分の民を裁かれるだろう」。
\v 31 生ける神のみ手に落ちるのは恐ろしいことです。
\v 32 しかし，以前の日々のことを思い出しなさい。あなた方は，啓発を受けたのち，苦しみを伴う厳しい戦いを耐え忍びました。
\v 33 ある時には，非難と圧迫の両方にさらされ，またある時には，そのように扱われた人たちと共にあずかる者になりました。
\v 34 あなた方は，鎖につながれているわたしに哀れみを抱いてくれましたし，自分の財産が略奪されても喜んで甘受したのです。自分たちには，さらに優れた，永続する財産が天にあることを知っていたからです。
\v 35 ですから，あなた方は自分の大胆さを投げ捨ててはいけません。それには大きな報いがあります。
\v 36 というのは，神のご意志を行なって約束のものを受けるため，あなた方には忍耐が必要だからです。
\q1
\v 37 「ほんのしばらくすれば，
\q2 来たるべき方が来られるだろう。遅れることはない。
\q1
\v 38 だが，義人は信仰によって生きるだろう。
\q2 彼がたじろぐなら，わたしの魂は彼を喜ばない」。
\p
\v 39 しかし，わたしたちは，たじろいで滅びに至る者ではなく，信仰を抱いて魂の救いに至る者です。
\p
\c 11
\v 1 さて，信仰とは，望んでいる事柄についての保証であり，見ていない事柄についての証明です。
\v 2 これによって昔の人たちは証言を得たのです。
\v 3 信仰によって，わたしたちは，全宇宙は神の言葉によって造られたのであり，それゆえ，見えるものは目に見えるものからできたのではないことが分かります。
\v 4 信仰によって，アベルはカインよりも優れた犠牲を神にささげ，その信仰によって，彼が義人であるという証言をその方から受けました。神が彼の供え物について証言されたからです。その信仰によって，彼は死んではいても，なお語っているのです。
\v 5 信仰によって，エノクは死を見ないように取り去られました。神が彼を移されたので，彼は見いだされなくなりました。というのは，移される前に，神を十分に喜ばせていたことが証言されていたからです。
\v 6 信仰がなければ，その方を十分に喜ばせることは不可能です。神に近づく者は，その方が存在し，ご自分を求める者たちに報いてくださることを信じるべきだからです。
\v 7 信仰によって，ノアは，まだ見ていない事柄について警告を受けたとき，信心深い恐れに動かされて，自分の家族の救いのために箱船を設けました。その信仰によって，彼は世を罪に定め，信仰による義の相続人となりました。
\v 8 信仰によって，アブラハムは，自分が相続財産として受けることになる場所に出てゆくよう召された時，それに従いました。自分がどこに行くのかを知らずに出て行ったのです。
\v 9 信仰によって，彼は異国人のように，自分のものではない土地にいるようにして，約束の地に住みました。同じ約束を共に相続するイサクやヤコブと共に，天幕に住んだのです。
\v 10 なぜなら，彼は，基礎を据えられた都市を待ち望んでいたからです。その都市の建設者また造り主は神です。
\v 11 信仰によって，サラ自身までも妊娠する力を受け，そういった年齢を過ぎていたのに子供を産みました。彼女は，約束してくださった方を誠実な方とみなしていたからです。
\v 12 このようにして，この死んでいたも同然の人から，数の多さにおいては天の星のように多数の，また海辺の砂のように無数の子孫が生まれたのです。
\v 13 これらの人はみな，信仰のうちに死にました。約束のものを受けませんでしたが，遠くからそれらを見て\f + TRは言葉を加えて次のようにしている。「見て，それらを確信して」\f*喜んで迎え，自分たちが地上では他国人また居留者であることを告白したのです。
\v 14 というのは，このように言う人たちは，自分たちが故郷を探し求めていることを明らかにしているからです。
\v 15 確かに，もし彼らが自分たちの出て来た故郷のことを思っていたのであれば，帰る機会もあったことでしょう。
\v 16 しかし今，彼らはさらにまさった故郷，つまり天にある故郷を熱望しています。それゆえ，神は，彼らの神と呼ばれることを恥とはされません。彼らのために都市を準備されたからです。
\p
\v 17 信仰によって，アブラハムは，試みを受けていた時，イサクをささげました。そうです，約束を喜びのうちに受けていた彼が，自分のひとり子をささげようとしたのです。
\v 18 しかも彼に対しては，「あなたの子孫はイサクにあって呼ばれることになる」と言われていたのです。
\v 19 彼は，神は死んだ者たちの中からさえも起こすことがおできになると考えました。比ゆ的に言って，彼はイサクを死んだ者たちの中から取り戻したのです。
\v 20 信仰によって，イサクは，来たるべき事柄についても，ヤコブとエサウを祝福しました。
\v 21 信仰によって，ヤコブは，死にかけていた時，ヨセフの子らをそれぞれ祝福し，つえの先のところにもたれて崇拝しました。
\v 22 信仰によって，ヨセフは，自分の最期が近づいていた時，イスラエルの子らの脱出のことに言及し，自分の骨について指図しました。
\v 23 信仰によって，モーセは生まれた時，その両親によって三か月の間隠されました。彼らは彼が美しい子供であるのを見て，王の命令を恐れなかったのです。
\v 24 信仰によって，モーセは，成長した時，ファラオの娘の子と呼ばれるのを拒みました。
\v 25 罪の一時的な快楽を持つよりも，神の民と共に虐待されることを選んだのであり，
\v 26 キリストの非難をエジプトの宝にまさる富だと考えたのです。報いに目を向けていたからです。
\v 27 信仰によって，彼は，王の憤りを恐れず，エジプトを去りました。見えない方を見ているようにして，耐え忍んでいたからです。
\v 28 信仰によって，初子を滅ぼす者が自分たちに触れないよう，過ぎ越しと血の振り注ぎを行ないました。
\v 29 信仰によって，彼らは乾いた陸地のようにして紅海を通り抜けました。エジプト人が同じことを試みると，のみ込まれました。
\v 30 信仰によって，エリコの城壁は，彼らが周囲を七日間回った後に崩れ落ちました。
\v 31 信仰によって，売春婦ラハブは，スパイたちを平和のうちに迎え入れたので，不従順な者たちと共に滅びることはありませんでした。
\v 32 この上わたしは何を言いましょうか。ギデオン，バラク，サムソン，エフタ，ダビデ，サムエル，また預言者たちについて語るなら，時間がわたしを置いていってしまうでしょう。
\v 33 彼らは，信仰によって，数々の王国を征服し，義を成し遂げ，約束のものを獲得し，ライオンたちの口をふさぎ，
\v 34 火の力を消し，剣の刃から逃れ，弱かったのに強い者とされ，戦いにおいて強力な者となり，異国の軍勢を敗退させました。
\v 35 女たちはその死者を復活によって受けました。ほかの人たちは，さらにまさった復活を獲得するために，釈放を受け入れずに拷問されました。
\v 36 別の人たちは，あざけりやむち打ちによって試みられ，そればかりか，束縛や投獄によっても試みられました。
\v 37 彼らは石打ちにされました。のこぎりで切られました。誘惑を受けました。剣で殺されました。羊の皮やヤギの皮をまとって巡り歩き，窮乏し，虐待を受け
\v 38 （彼らにはこの世はふさわしくなかったのです），荒野，山々，ほら穴，地の穴をさまよい続けました。
\v 39 これらの人々は皆，その信仰によって証言されましたが，約束のものは受けませんでした。
\v 40 神は，わたしたちのためにさらにまさったものを備えて，わたしたちを抜きにしては彼らが完全にされることのないようにされたのです。
\p
\c 12
\v 1 それゆえ，わたしたちも，これほど大勢の雲のような証人たちに囲まれているのですから，あらゆる重荷と，すぐに絡みつく罪とを捨てて，わたしたちの前に置かれた競走を忍耐して走ろうではありませんか。
\v 2 信仰の創始者また完成者であるイエスを見つめながら。彼は，自分の前に置かれた喜びのゆえに，恥をものともせずに十字架を耐え忍び，神のみ座の右に座られたのです。
\v 3 罪人たちによるこのような反抗に耐え忍ばれた方のことをよく考えなさい。あなた方が疲れ，あなた方の魂が弱り果てることのないためです。
\v 4 あなた方は罪と闘うにあたって，まだ血に至るまで抵抗したことがありません。
\v 5 そして，子供たちに対するようにしてあなた方に語られている，次のような勧めを忘れてしまっています。
\q1 「わが子よ，主の懲らしめを軽んじてはならず，
\q2 その方から戒められるとき，弱り果ててもいけない。
\q1
\v 6 主はご自分の愛する者を懲らしめ，
\q2 受け入れる子をすべてむち打たれるからだ」。
\p
\v 7 あなた方が耐え忍んでいるのは鍛練のためです。神はあなた方を子供たちのように扱っておられます。というのは，父親が鍛練しない子などいるでしょうか。
\v 8 しかし，みんなが共にあずかっている鍛錬をあなた方が受けていないとすれば，あなた方は私生児であって，子ではありません。
\v 9 さらにまた，わたしたちを懲らしめるために，同じく肉の父親たちがいて，わたしたちは彼らに敬意を表わしていました。霊の父にはなおのこと，服従して生きるべきではないでしょうか。
\v 10 というのは，確かに父親たちは，少しばかりの日々，自分が良いと思うままにわたしたちを罰しましたが，その方は，わたしたちの益のため，わたしたちがご自分の神聖さに共にあずかる者となるために罰してくださるのです。
\v 11 懲らしめはすべて，その時は喜ばしいことではなく，つらいことに思えますが，後になれば，それによって訓練された人たちに，義という平和な実を生み出すのです。
\v 12 ですから，垂れ下がった手と衰えたひざをまっすぐにしなさい。
\v 13 そして，不自由な足が脱臼することなく，むしろいやされるように，あなた方の足のためにまっすぐな道を造りなさい。
\v 14 すべての人たちとの平和を追い求めなさい。また聖化を追い求めなさい。それなくしてはだれも主を見ることはできません。
\v 15 神の恵みに達していない人が出ることのないよう，また，苦い根が伸びてきてあなた方を悩まし，それによって多くの者たちが汚されることのないよう，よく注意しなさい。
\v 16 一度の食事のために自分の長子の権利を売ったエサウのように，淫行の者や神聖さを汚す者にならないようにしなさい。
\v 17 というのは，彼が後から祝福を受け継ぎたいと願ったのに退けられたことを，あなた方は知っているからです。彼は，涙をもってそれを切に求めましたが，思いの変化の余地を見いだせなかったのです。
\v 18 あなた方が近づいているのは，触れることのできる，火で燃えている山や，暗黒，暗闇，暴風雨，
\v 19 らっぱの響き，言葉の声の前ではありません。それを聞いた者たちは，これ以上自分たちに言葉が語られることのないように懇願しました。
\v 20 彼らは，「獣であっても山に触れるなら，それは石打ちにされ\f + TRは「るか，矢で射られ」を加えている。[出エジプト 19:12-13を見よ]\f*なければならない」と命じられたことに耐えられなかったのです。
\v 21 また，その光景があまりにも恐ろしかったので，モーセは，「わたしはおびえ，震えている」と言いました。
\p
\v 22 しかし，あなた方が近づいているのは，シオンの山，また生ける神の都，すなわち天のエルサレム，そして無数のみ使いたち，
\v 23 すなわち全体の集会，また天で登録されている初子たちの集会，すべての人の裁き主である神，完全にされた正しい人たちの霊，
\v 24 新しい契約の仲介者イエス，さらに，アベルの血よりも立派に語る，振り注ぎの血の前なのです。
\p
\v 25 語っている方を拒まないように気をつけなさい。地上で警告していた人を拒んだ人々が逃れられなかったのであれば，天から警告される方に背を向けるわたしたちは，なおさら逃れることはできないでしょう。
\v 26 その時，その方の声は地を揺り動かしましたが，今や，次のように約束されています。「わたしはもう一度，地だけではなく，天をも揺り動かすだろう」。
\v 27 この「もう一度」という表現は，揺り動かされないものが残るために，揺り動かされるものが，造られたものとして取り除かれることを示しています。
\v 28 それゆえ，わたしたちは，揺り動かされることのない王国を受けようとしているのですから，感謝の念を抱きましょう。それによって崇敬と畏敬の念をもって，受け入れられる仕方で神に仕えるのです。
\v 29 わたしたちの神は焼き尽くす火だからです。
\p
\c 13
\v 1 兄弟愛を保ちなさい。
\v 2 よそから来た人を親切にもてなすことを忘れてはいけません。そうすることにより，ある人たちはそれと知らずにみ使いたちをもてなしたのです。
\v 3 獄につながれている人たちのことを，彼らと共につながれているようにして覚えておきなさい。また，虐待されている人たちのことを覚えておきなさい。あなた方も肉体でいるからです。
\v 4 すべての人たちの間で結婚が尊ばれるようにし，寝床が汚されないようにしなさい。神は，淫行の者たちや姦淫を犯す者たちを裁かれるのです。
\p
\v 5 お金に対する愛から自由になり，自分の持っているもので満足しなさい。「わたしは決してあなたを離れず，決してあなたを見捨てない」と言われているからです。
\v 6 それで，わたしたちは確信をもって次のように言います。
\q1 「主はわたしの助け主。わたしは恐れない。
\q2 人がわたしに何をできようか」。
\p
\v 7 あなた方の指導者たち，あなた方に神の言葉を語った人たちのことを覚えておきなさい。彼らの行ないの結末を注意深く見て，彼らの信仰に倣いなさい。
\v 8 イエス・キリストは，昨日も，今日も，そして永久に同じ方です。
\v 9 さまざまな異なった教えによって運び去られてはいけません。心は，食物ではなく，恵みによって強められるのが良いことだからです。食物に気を取られていた人たちは益を得ませんでした。
\p
\v 10 わたしたちには一つの祭壇があります。それは，聖なる幕屋に仕えている人たちはそこから食べる権利のないものです。
\v 11 なぜなら，動物の血は罪のためのささげ物として大祭司によって聖なる場所に持って行かれますが，その体は宿営の外で焼かれるからです。
\v 12 そのため，イエスも，ご自分の血によって民を聖化するために，門の外で苦しみを受けられました。
\v 13 ですから，わたしたちは彼の恥辱を担って，宿営の外に出て彼のもとに行こうではありませんか。
\v 14 わたしたちは，ここには永続する都市を持っておらず，来たるべき都市を得ようとしているからです。
\v 15 それで，彼を通して，絶えず神に賛美の犠牲を，すなわちそのみ名を告白する唇の実をささげましょう。
\v 16 しかし，善い行ない，そして分け合うことを忘れてはいけません。そのような犠牲こそ，神は喜ばれるからです。
\p
\v 17 あなた方の指導者たちに従い，また服しなさい。彼らはやがて言い開きをする者として，あなた方の魂のために見張りをしているからです。彼らが，うめき声ではなく喜びをもって，このことを行なえるようにしなさい。そうでないと，あなた方の益になりません。
\p
\v 18 わたしたちのために祈ってください。わたしたちは，すべてのことにおいて高潔に生活したいと願っており，自分が正しい良心を持っていると確信しているからです。
\v 19 わたしはあなた方がこのことをしてくれるよう特にお願いします。わたしがなるべく早くあなた方のところに帰れるようになるためです。
\p
\v 20 それでは，平和の神，すなわち，永遠の契約の血による羊の偉大な牧者であるわたしたちの主イエスを死んだ者たちの中から連れ戻された方が，
\v 21 イエス・キリストを通して，み前にあってみ心にかなうことをあなた方のうちで行なってくださり，あなた方がそのご意志を行なうため，あらゆる善い業において整えてくださいますように。この方に，栄光がいつまでもありますように。アーメン。
\p
\v 22 兄弟たち，わたしはあなた方に，以上の勧告の言葉を我慢してくれるように勧めます。わたしは少しの言葉であなた方に書き記したからです。
\v 23 わたしたちの兄弟テモテが釈放されたことを知ってください。彼がすぐに来れば，わたしは彼と共にあなた方に会えるでしょう。
\v 24 あなた方の指導者たち全員と，すべての聖徒たちによろしく伝えてください。イタリアの人たちがあなた方によろしくと言っています。
\v 25 恵みが，あなた方すべてと共にありますように。アーメン。
\b
