#!/bin/bash
for FILE in `ls *.html` ; do

html2text -o $FILE.txt $FILE
done

for FILE in `ls *.txt` ; do
	sed -i '1,16d' $FILE
	sed -e :a -e '$d;N;2,7ba' -e 'P;D' $FILE >$FILE.usfm
	
done

 for FILE in `ls *.usfm` ; do
	rename 's/html.txt.usfm/usfm/g' $FILE
 
done
rm *.txt
 
