#!/bin/bash
 for FILE in `ls *.usfm` ; do

	sed -ri 's/^\*.* Chapter ([0-9]*) \**$/\\c \1\n\\p/g' $FILE
	sed -ri 's/^\s\s[0-9]*:/\\v /g' $FILE
	##Supprime les retours à la ligne et debut de ligne de 5 espaces
	sed -i ':a;N;$!ba;s/\n\s\s\s\s\s//g' $FILE
	##Ajoute les lignes de début d'identification usfm
	sed -e '1i \\\id \n\\h \n\\toc1 \n\\toc2 \n\\toc3 ' $FILE >$FILE.txt
done
rm *.usfm
for FILE in `ls *.txt` ; do
	rename 's/usfm.txt/usfm/g' $FILE 
	#sed -i's/^id/\\&/' $FILE 
done
 
